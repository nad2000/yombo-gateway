Logreader Example
==================

Monitors a text file. Any lines are sent to the yombobot module for
processing.

A great example of how a simple logic module can be combined to
extend the capabilites of another modules.  Also, a great way
to show how simple FileReader can be.

Installation
============

Simply mark this module as being used by the gateway, and the gateway will
download this module and install this module automatically.

Requirements
============

The Yombobot module is required to be installed and running.

License
=======

Same license as Yombo Gateway.

