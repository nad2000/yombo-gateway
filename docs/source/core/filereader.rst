.. index:: filereader

.. _filereader:

.. currentmodule:: yombo.core.filereader

============================================
FileReader (yombo.core.filereader)
============================================

.. automodule:: yombo.core.filereader
   :members:
