.. index:: auth

.. _auth:

.. currentmodule:: yombo.core.auth

==============================
Auth (yombo.core.auth)
==============================

.. automodule:: yombo.core.auth
   :members:
