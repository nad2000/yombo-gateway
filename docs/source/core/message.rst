.. index:: message

.. _message:

.. currentmodule:: yombo.core.message

==========================================
Message (yombo.core.message)
==========================================
.. automodule:: yombo.core.message

Message Class
============================
.. autoclass:: Message
   :members:

   .. automethod:: __init__
