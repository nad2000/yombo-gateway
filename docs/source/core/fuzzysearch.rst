.. index:: fuzzysearch

.. _fuzzysearch:

.. currentmodule:: yombo.core.fuzzysearch

============================================
FuzzySearch (yombo.core.fuzzysearch)
============================================

.. automodule:: yombo.core.fuzzysearch
   :members:
