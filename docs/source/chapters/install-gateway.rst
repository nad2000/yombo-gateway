.. prepare-operating-system:

##################
Installing Gateway
##################

This is the second step to installing the Yombo Gateway software.  If you have
not already done, be sure to 
:doc:`install python and required modules <prepare-operating-system>` first.

There are two methods to getting the the software:

1. :doc:`Basic download of repository. <../install/install-gateway-basic>`

  * Use this if you want to quickly download the software and get started.
  * Not able to easily contribute code back to the community.
  
2. :doc:`Fork the gateway repository. <../install/install-gateway-fork>`

  * The gateway software is stored in a git repository. Simply fork it, download it, and run.
  * Can easily create pull requests to contribute code back to the community.

