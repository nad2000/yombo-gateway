.. index:: logwriter

.. _logwriter:

.. currentmodule:: yombo.modules.logwriter

The logwriter module demonstrates subscribing to :ref:`messages <message>` .

For more on developing modules, visit
`Projects.Yombo.net <http://projects.yombo.net/projects/modules>`_ .

==========================================
Empty Module (yombo.modules.logwriter)
==========================================
.. automodule:: yombo.modules.logwriter

Empty  Class
============================
.. autoclass:: logwriter
   :members:

