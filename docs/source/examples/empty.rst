.. index:: emptymodule

.. _emptymodule:

.. currentmodule:: yombo.modules.empty

The empty module is a basic shell of a module. This module fully works, however
it doesn't do very much. This module is typically used for creating a new
module...or a template.

For more on developing modules, visit
`Projects.Yombo.net <http://projects.yombo.net/projects/modules>`_ .

==========================================
Empty Module (yombo.modules.empty)
==========================================
.. automodule:: yombo.modules.empty

Empty  Class
============================
.. autoclass:: Empty
   :members:

