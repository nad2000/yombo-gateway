.. index:: loader

.. _loader:

.. currentmodule:: yombo.lib.loader

==========================
Loader (yombo.lib.loader)
==========================
.. automodule:: yombo.lib.loader

Loader Class
==============
.. autoclass:: Loader
   :members:

   .. automethod:: __init__
