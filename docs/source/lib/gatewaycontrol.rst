.. index:: gatewaycontrol

.. _gateway_control:

.. currentmodule:: yombo.lib.gatewaycontrol

===========================================
Gateway Control (yombo.lib.gatewaycontrol)
===========================================
.. automodule:: yombo.lib.gatewaycontrol

GatewayControl class
===========================================
.. autoclass:: GatewayControl
   :members:

   .. automethod:: __init__
