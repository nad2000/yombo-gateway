.. index:: times

.. _times::

.. currentmodule:: yombo.lib.times

=======================
Times (yombo.lib.times)
=======================
.. automodule:: yombo.lib.times

Times Class
=======================
.. autoclass:: Times
   :members:

   .. automethod:: __init__
