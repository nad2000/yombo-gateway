.. index:: configuration

.. _configuration:

.. currentmodule:: yombo.lib.configuration

========================================
Configuration (yombo.lib.configuration)
========================================
.. automodule:: yombo.lib.configuration

Configuration Class
=======================
.. autoclass:: Configuration
   :members:

   .. automethod:: __init__
